
# Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
# Click nbfs://nbhost/SystemFileSystem/Templates/Other/Dockerfile to edit this template

FROM openjdk:8-jdk-alpine
RUN addgroup -S bpichincha && adduser -S bpichincha -G bpichincha
USER bpichincha:bpichincha
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} bcopic-transaccion-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/bcopic-transaccion-0.0.1-SNAPSHOT.jar"]