/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author christian
 */
@Entity
@Table(name = "tbl_parametro")
@Data
public class Parametro implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "par_id")
    private Long id;
    
    @Column(name = "par_codigo", length = 20)
    @NotNull
    private String codigo;

    @Column(name = "par_nombre", length = 50)
    @NotNull
    private String nombre;

    @Column(name = "par_valor", length = 50)
    @NotNull
    private String valor;
}
