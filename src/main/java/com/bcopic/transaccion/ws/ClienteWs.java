/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.ws;

import com.bcopic.transaccion.constante.ConstanteSistema;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Cliente;
import com.bcopic.transaccion.servicio.ClienteServicio;
import com.bcopic.transaccion.to.RespuestaTo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author christian
 */
@RestController
@Slf4j
@RequestMapping("/clientes")
public class ClienteWs {

    @Autowired
    private ClienteServicio clienteServicio;

    @PostMapping("/guardar")
    public ResponseEntity<RespuestaTo> guardar(@RequestBody Cliente cliente) {
        try {
            clienteServicio.guardar(cliente);
            return ResponseEntity.ok(new RespuestaTo(ConstanteSistema.CORRECTO));
        } catch (SistemaExcepcion | RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

    @PutMapping("/actualizar/{id}")
    public ResponseEntity<RespuestaTo> actualizar(@RequestBody Cliente cliente, @PathVariable Long id) {
        try {
            clienteServicio.actualizar(cliente, id);
            return ResponseEntity.ok(new RespuestaTo(ConstanteSistema.CORRECTO));
        } catch (SistemaExcepcion | RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<RespuestaTo> eliminar(@PathVariable Long id) {
        try {
            clienteServicio.eliminar(id);
            return ResponseEntity.ok(new RespuestaTo(ConstanteSistema.CORRECTO));
        } catch (SistemaExcepcion | RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

    @GetMapping("/listarTodo")
    public ResponseEntity<RespuestaTo> listarTodo() {
        try {
            RespuestaTo respuesta = new RespuestaTo();
            respuesta.setMensaje(ConstanteSistema.CORRECTO);
            respuesta.setListaDatos(clienteServicio.listarTodo());
            return ResponseEntity.ok(respuesta);
        } catch (RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

    @GetMapping("/listarActivo")
    public ResponseEntity<RespuestaTo> listarActivo() {
        try {
            RespuestaTo respuesta = new RespuestaTo();
            respuesta.setMensaje(ConstanteSistema.CORRECTO);
            respuesta.setListaDatos(clienteServicio.listarActivo());
            return ResponseEntity.ok(respuesta);
        } catch (SistemaExcepcion | RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

}
