/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.ws;

import com.bcopic.transaccion.constante.ConstanteSistema;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Movimiento;
import com.bcopic.transaccion.servicio.MovimientoServicio;
import com.bcopic.transaccion.to.RespuestaTo;
import com.bcopic.transaccion.util.FechaUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author christian
 */
@RestController
@Slf4j
@RequestMapping("/movimientos")
public class MovimientoWs {

    @Autowired
    private MovimientoServicio movimientoServicio;

    @PostMapping("/guardar")
    public ResponseEntity<RespuestaTo> guardar(@RequestBody Movimiento movimiento) {
        try {
            movimientoServicio.guardar(movimiento);
            return ResponseEntity.ok(new RespuestaTo(ConstanteSistema.CORRECTO));
        } catch (SistemaExcepcion | RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

    @GetMapping("/listarTodo")
    public ResponseEntity<RespuestaTo> listarTodo() {
        try {
            RespuestaTo respuesta = new RespuestaTo();
            respuesta.setMensaje(ConstanteSistema.CORRECTO);
            respuesta.setListaDatos(movimientoServicio.listarTodo());
            return ResponseEntity.ok(respuesta);
        } catch (SistemaExcepcion | RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

    @GetMapping("/listarReportePorIdentificacionFecha/{identificacion}/{fechaDesde}/{fechaHasta}")
    public ResponseEntity<RespuestaTo> listarReportePorIdentificacionFecha(@PathVariable String identificacion,
            @PathVariable String fechaDesde, @PathVariable String fechaHasta) {
        try {
            RespuestaTo respuesta = new RespuestaTo();
            respuesta.setMensaje(ConstanteSistema.CORRECTO);
            respuesta.setListaDatos(movimientoServicio.listarPorFechaIdentificacion(FechaUtil.obtenerFechaDesde(FechaUtil.transformatoStringAFecha("yyyy-MM-dd", fechaDesde)), FechaUtil.obtenerFechaHasta(FechaUtil.transformatoStringAFecha("yyyy-MM-dd", fechaHasta)),
                    identificacion));
            return ResponseEntity.ok(respuesta);
        } catch (SistemaExcepcion | RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

}
