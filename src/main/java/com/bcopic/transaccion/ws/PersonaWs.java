/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.ws;

import com.bcopic.transaccion.constante.ConstanteSistema;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Persona;
import com.bcopic.transaccion.servicio.PersonaServicio;
import com.bcopic.transaccion.to.RespuestaTo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author christian
 */
@RestController
@Slf4j
@RequestMapping("/persona")
public class PersonaWs {

    @Autowired
    private PersonaServicio personaServicio;

    @PostMapping("/guardar")
    public ResponseEntity<RespuestaTo> guardar(@RequestBody Persona persona) {
        try {
            personaServicio.guardar(persona);
            return ResponseEntity.ok(new RespuestaTo(ConstanteSistema.CORRECTO));
        } catch (SistemaExcepcion | RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

}
