/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.ws;

import com.bcopic.transaccion.constante.ConstanteSistema;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Cuenta;
import com.bcopic.transaccion.servicio.CuentaServicio;
import com.bcopic.transaccion.to.RespuestaTo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author christian
 */
@RestController
@Slf4j
@RequestMapping("/cuentas")
public class CuentaWs {

    @Autowired
    private CuentaServicio cuentaServicio;

    @PostMapping("/guardar")
    public ResponseEntity<RespuestaTo> guardar(@RequestBody Cuenta cuenta) {
        try {
            cuentaServicio.guardar(cuenta);
            return ResponseEntity.ok(new RespuestaTo(ConstanteSistema.CORRECTO));
        } catch (SistemaExcepcion | RuntimeException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

    @PutMapping("/actualizar/{id}")
    public ResponseEntity<RespuestaTo> actualizar(@RequestBody Cuenta cuenta, @PathVariable Long id) {
        try {
            cuentaServicio.actualizar(cuenta, id);
            return ResponseEntity.ok(new RespuestaTo(ConstanteSistema.CORRECTO));
        } catch (SistemaExcepcion | RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<RespuestaTo> eliminar(@PathVariable Long id) {
        try {
            cuentaServicio.eliminar(id);
            return ResponseEntity.ok(new RespuestaTo(ConstanteSistema.CORRECTO));
        } catch (SistemaExcepcion | RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

    @GetMapping("/listarTodo")
    public ResponseEntity<RespuestaTo> listarTodo() {
        try {
            RespuestaTo respuesta = new RespuestaTo();
            respuesta.setMensaje(ConstanteSistema.CORRECTO);
            respuesta.setListaDatos(cuentaServicio.listarTodo());
            return ResponseEntity.ok(respuesta);
        } catch (RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

    @GetMapping("/listarActivo")
    public ResponseEntity<RespuestaTo> listarActivo() {
        try {
            RespuestaTo respuesta = new RespuestaTo();
            respuesta.setMensaje(ConstanteSistema.CORRECTO);
            respuesta.setListaDatos(cuentaServicio.listarActivo());
            return ResponseEntity.ok(respuesta);
        } catch (SistemaExcepcion | RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(new RespuestaTo(e.getMessage()));
        }
    }

}
