/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.servicio;

import com.bcopic.transaccion.constante.ConstanteSistema;
import com.bcopic.transaccion.dao.ClienteDao;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Cliente;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author christian
 */
@Service
public class ClienteServicio {

    @Autowired
    private ClienteDao clienteDao;

    @Transactional
    public void guardar(Cliente cliente) throws SistemaExcepcion {
        clienteDao.guardar(cliente);
    }

    @Transactional
    public void actualizar(Cliente cliente, Long id) throws SistemaExcepcion {
        Cliente entidad = clienteDao.obtenerPorLlavePrimaria(id);
        if (Objects.isNull(entidad)) {
            clienteDao.guardar(entidad);
        } else {
            cliente.setId(id);
            cliente.getPersona().setId(entidad.getPersona().getId());
            clienteDao.guardar(cliente);
        }
    }

    @Transactional
    public void eliminar(Long id) throws SistemaExcepcion {
        Cliente entidad = clienteDao.obtenerPorLlavePrimaria(id);
        if (Objects.isNull(entidad)) {
            throw new SistemaExcepcion(ConstanteSistema.REGISTRO_NO_EXISTE);
        } else {
            entidad.setEstado(Boolean.FALSE);
            clienteDao.guardar(entidad);
        }
    }

    public List<Cliente> listarTodo() {
        return clienteDao.listarTodo();
    }

    public List<Cliente> listarActivo() throws SistemaExcepcion {
        return clienteDao.listarActivo();
    }

    public Cliente obtenerPorIdentificacion(String identificacion) throws SistemaExcepcion {
        return clienteDao.obtenerPorIdentificacion(identificacion);
    }
}
