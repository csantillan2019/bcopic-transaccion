/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.servicio;

import com.bcopic.transaccion.dao.ParametroDao;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Parametro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author christian
 */
@Service
public class ParametroServicio {

    @Autowired
    private ParametroDao parametroDao;

    public Parametro obtenerPorCodigo(String codigo) throws SistemaExcepcion {
        return parametroDao.obtenerPorCodigo(codigo);
    }

}
