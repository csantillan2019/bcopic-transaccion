/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.servicio;

import com.bcopic.transaccion.constante.ConstanteSistema;
import com.bcopic.transaccion.dao.CuentaDao;
import com.bcopic.transaccion.dao.MovimientoDao;
import com.bcopic.transaccion.enumeracion.CatalogoEnum;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Catalogo;
import com.bcopic.transaccion.modelo.Cliente;
import com.bcopic.transaccion.modelo.Cuenta;
import com.bcopic.transaccion.modelo.Movimiento;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author christian
 */
@Service
public class CuentaServicio {

    @Autowired
    private CuentaDao cuentaDao;
    @Autowired
    private ClienteServicio clienteServicio;
    @Autowired
    private CatalogoServicio catalogoServicio;
    @Autowired
    private MovimientoDao movimientoDao;

    @Transactional
    public void guardar(Cuenta cuenta) throws SistemaExcepcion {
        Cliente cliente = clienteServicio.obtenerPorIdentificacion(cuenta.getCliente().getIdentificacion());
        Catalogo tipo = catalogoServicio.obtenerPorCodigo(cuenta.getTipo().getCodigo());
        if (Objects.isNull(cliente)) {
            throw new SistemaExcepcion("Cliente no existe.");
        } else {
            cuenta.setCliente(cliente);
            cuenta.setTipo(tipo);
            Cuenta cuentaId = cuentaDao.guardarRetorno(cuenta);
            
            System.out.println("---"+cuentaId.getId());

            Movimiento movimiento = new Movimiento();
            Catalogo tipoMovimientoDeposito = catalogoServicio.obtenerPorCodigo(CatalogoEnum.TIPO_MOVIMIENTO_DEPOSITO.getNemonico());
            movimiento.setCuenta(cuentaId);
            movimiento.setFecha(new Date());
            movimiento.setTipo(tipoMovimientoDeposito);
            movimiento.setSaldo(cuenta.getSaldoInicial());
            movimiento.setValor(cuenta.getSaldoInicial());
            movimientoDao.guardar(movimiento);
        }
    }

    @Transactional
    public void actualizar(Cuenta cuenta, Long id) throws SistemaExcepcion {
        Cuenta entidad = cuentaDao.obtenerPorLlavePrimaria(id);
        Cliente cliente = clienteServicio.obtenerPorIdentificacion(cuenta.getCliente().getIdentificacion());
        Catalogo tipo = catalogoServicio.obtenerPorCodigo(cuenta.getTipo().getCodigo());
        if (Objects.isNull(entidad)) {
            cuentaDao.guardar(entidad);
        } else {
            cuenta.setId(id);
            cuenta.setCliente(cliente);
            cuenta.setTipo(tipo);
            cuentaDao.guardar(cuenta);
        }
    }

    @Transactional
    public void eliminar(Long id) throws SistemaExcepcion {
        Cuenta entidad = cuentaDao.obtenerPorLlavePrimaria(id);
        if (Objects.isNull(entidad)) {
            throw new SistemaExcepcion(ConstanteSistema.REGISTRO_NO_EXISTE);
        } else {
            entidad.setEstado(Boolean.FALSE);
            cuentaDao.guardar(entidad);
        }
    }

    public List<Cuenta> listarTodo() {
        return cuentaDao.listarTodo();
    }

    public List<Cuenta> listarActivo() throws SistemaExcepcion {
        return cuentaDao.listarActivo();
    }

    public Cuenta obtenerPorCliente(Cliente cliente) throws SistemaExcepcion {
        return cuentaDao.obtenerPorCliente(cliente);
    }
}
