/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.servicio;

import com.bcopic.transaccion.dao.PersonaDao;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author christian
 */
@Service
public class PersonaServicio {

    @Autowired
    private PersonaDao personaDao;

    @Transactional
    public void guardar(Persona persona) throws SistemaExcepcion {
        personaDao.guardar(persona);
    }

}
