/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.servicio;

import com.bcopic.transaccion.constante.ConstanteSistema;
import com.bcopic.transaccion.dao.MovimientoDao;
import com.bcopic.transaccion.enumeracion.CatalogoEnum;
import com.bcopic.transaccion.enumeracion.ParametroEnum;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Catalogo;
import com.bcopic.transaccion.modelo.Cliente;
import com.bcopic.transaccion.modelo.Cuenta;
import com.bcopic.transaccion.modelo.Movimiento;
import com.bcopic.transaccion.modelo.Parametro;
import com.bcopic.transaccion.to.ReporteTo;
import com.bcopic.transaccion.util.FechaUtil;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author christian
 */
@Service
public class MovimientoServicio {

    @Autowired
    private MovimientoDao movimientoDao;
    @Autowired
    private CuentaServicio cuentaServicio;
    @Autowired
    private ClienteServicio clienteServicio;
    @Autowired
    private CatalogoServicio catalogoServicio;
    @Autowired
    private ParametroServicio parametroServicio;

    @Transactional
    public void guardar(Movimiento movimiento) throws SistemaExcepcion {
        Cliente cliente = clienteServicio.obtenerPorIdentificacion(movimiento.getIdentificacionCliente());
        Cuenta cuenta = cuentaServicio.obtenerPorCliente(cliente);
        Catalogo tipoMovimiento = catalogoServicio.obtenerPorCodigo(movimiento.getTipo().getCodigo());
        Catalogo tipoMovimientoRetiro = catalogoServicio.obtenerPorCodigo(CatalogoEnum.TIPO_MOVIMIENTO_RETIRO.getNemonico());
        Movimiento movimientoEntidad = movimientoDao.obtenerUltimoPorCuenta(cuenta.getId());
        List<Movimiento> listaMovimientoRetiro = movimientoDao.listarPorCuentaFechaTipoMovimiento(cuenta.getId(), FechaUtil.obtenerFechaDesde(new Date()), FechaUtil.obtenerFechaHasta(new Date()), tipoMovimientoRetiro.getId());
        Parametro limiteRetiro = parametroServicio.obtenerPorCodigo(ParametroEnum.LIMITE_DIARIO_RETIRO.getCodigo());
        BigDecimal saldo = BigDecimal.ZERO;
        if (Objects.isNull(movimientoEntidad)) {
            movimientoEntidad = new Movimiento();
        } else {
            saldo = movimientoEntidad.getSaldo();
        }

        validarSaldosYretiro(listaMovimientoRetiro, movimiento, tipoMovimiento, limiteRetiro, saldo);

        if (tipoMovimiento.getCodigo().equals(CatalogoEnum.TIPO_MOVIMIENTO_DEPOSITO.getNemonico())) {
            saldo = saldo.add(movimiento.getValor());
        } else {
            saldo = saldo.subtract(movimiento.getValor());
        }
        movimientoEntidad = new Movimiento();
        movimientoEntidad.setCuenta(cuenta);
        movimientoEntidad.setFecha(new Date());
        movimientoEntidad.setSaldo(saldo);
        movimientoEntidad.setTipo(tipoMovimiento);
        movimientoEntidad.setValor(movimiento.getValor());
        movimientoDao.guardar(movimientoEntidad);
    }

    @Transactional
    public void guardar(Cuenta cuenta) throws SistemaExcepcion {
        Movimiento movimiento = new Movimiento();
        Catalogo tipoMovimientoDeposito = catalogoServicio.obtenerPorCodigo(CatalogoEnum.TIPO_MOVIMIENTO_DEPOSITO.getNemonico());
        movimiento.setCuenta(cuenta);
        movimiento.setFecha(new Date());
        movimiento.setTipo(tipoMovimientoDeposito);
        movimiento.setSaldo(cuenta.getSaldoInicial());
        movimiento.setValor(cuenta.getSaldoInicial());
        movimientoDao.guardar(movimiento);
    }

    private void validarSaldosYretiro(List<Movimiento> listaMovimiento, Movimiento movimiento, Catalogo tipoMovimiento, Parametro limiteRetiro, BigDecimal saldo) throws SistemaExcepcion {
        if (tipoMovimiento.getCodigo().equals(CatalogoEnum.TIPO_MOVIMIENTO_RETIRO.getNemonico())) {
            BigDecimal valorLimiteRetiro = new BigDecimal(limiteRetiro.getValor());
            if (Objects.isNull(listaMovimiento) || listaMovimiento.isEmpty()) {
                if (movimiento.getValor().compareTo(valorLimiteRetiro) == 1) {
                    throw new SistemaExcepcion(ConstanteSistema.LIMITE_RETIRO_EXCEDENTE + limiteRetiro.getValor());
                }
                BigDecimal saldos = saldo.subtract(movimiento.getValor());
                if (saldos.compareTo(BigDecimal.ZERO) == -1) {
                    throw new SistemaExcepcion(ConstanteSistema.SALDO_INSUFICIENTE);

                }

            } else {
                validarSaldosYretiro(listaMovimiento, movimiento, limiteRetiro, valorLimiteRetiro, saldo);
            }
        }
    }

    private void validarSaldosYretiro(List<Movimiento> listaMovimiento, Movimiento movimiento, Parametro limiteRetiro, BigDecimal valorLimiteRetiro, BigDecimal saldo) throws SistemaExcepcion {
        BigDecimal totalRetiroMovimiento = extraerRetirosDiarios(listaMovimiento, movimiento);
        BigDecimal saldos = saldo.subtract(movimiento.getValor());

        if (totalRetiroMovimiento.compareTo(valorLimiteRetiro) == 1) {
            throw new SistemaExcepcion(ConstanteSistema.LIMITE_RETIRO_EXCEDENTE + limiteRetiro.getValor());
        }
        if (saldos.compareTo(BigDecimal.ZERO) == -1) {
            throw new SistemaExcepcion(ConstanteSistema.SALDO_INSUFICIENTE);
        }
    }

    private BigDecimal extraerRetirosDiarios(List<Movimiento> listaMovimiento, Movimiento movimiento) {
        BigDecimal totalRetiro = BigDecimal.ZERO;
        if (Objects.nonNull(listaMovimiento) && !listaMovimiento.isEmpty()) {
            for (Movimiento mov : listaMovimiento) {
                totalRetiro = totalRetiro.add(mov.getValor());
            }
        }
        totalRetiro = totalRetiro.add(movimiento.getValor());
        return totalRetiro;
    }

    public List<Movimiento> listarTodo() throws SistemaExcepcion {
        return movimientoDao.listarTodo();
    }

    public List<ReporteTo> listarPorFechaIdentificacion(Date fechaDesde, Date fechaHasta, String identificacion) throws SistemaExcepcion {
        return movimientoDao.listarPorFechaIdentificacion(fechaDesde, fechaHasta, identificacion);
    }

}
