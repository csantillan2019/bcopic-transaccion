/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.servicio;

import com.bcopic.transaccion.dao.CatalogoDao;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Catalogo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author christian
 */
@Service
public class CatalogoServicio {

    @Autowired
    private CatalogoDao catalogoDao;

    public Catalogo obtenerPorCodigo(String codigo) throws SistemaExcepcion {
        return catalogoDao.obtenerPorCodigo(codigo);
    }

}
