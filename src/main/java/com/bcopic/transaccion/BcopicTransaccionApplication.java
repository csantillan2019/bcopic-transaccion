package com.bcopic.transaccion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BcopicTransaccionApplication {

	public static void main(String[] args) {
		SpringApplication.run(BcopicTransaccionApplication.class, args);
	}

}
