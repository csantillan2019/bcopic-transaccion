/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.enumeracion;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author christian
 */
public enum CatalogoEnum {

    TIPO_MOVIMIENTO_DEPOSITO("TMOVDEPOSITO"), TIPO_MOVIMIENTO_RETIRO("TMOVRETIRO");

    @Getter
    @Setter
    private String nemonico;

    private CatalogoEnum(String nemonico) {
        this.nemonico = nemonico;
    }

}
