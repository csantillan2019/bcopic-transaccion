/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.enumeracion;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author christian
 */
public enum ParametroEnum {

    LIMITE_DIARIO_RETIRO("LDRETIRO");

    @Getter
    @Setter
    private String codigo;

    private ParametroEnum(String codigo) {
        this.codigo = codigo;
    }

}
