/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.to;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author christian
 */
@Data
public class ReporteTo implements Serializable {

    private String fecha;
    private String cliente;
    private String numeroCuenta;
    private String tipo;
    private String saldoInicial;
    private String estado;
    private String movimiento;
    private String valor;
    private String saldoDisponible;

    public ReporteTo(String fecha, String cliente, String numeroCuenta, String tipo, String saldoInicial, String estado, String movimiento, String valor, String saldoDisponible) {
        this.fecha = fecha;
        this.cliente = cliente;
        this.numeroCuenta = numeroCuenta;
        this.tipo = tipo;
        this.saldoInicial = saldoInicial;
        this.estado = estado;
        this.movimiento = movimiento;
        this.valor = valor;
        this.saldoDisponible = saldoDisponible;
    }

}
