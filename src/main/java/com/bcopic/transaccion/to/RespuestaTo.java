/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.to;

import java.io.Serializable;
import java.util.List;
import lombok.Data;

/**
 *
 * @author christian
 */
@Data
public class RespuestaTo<T> implements Serializable {

    private List<T> listaDatos;

    public RespuestaTo() {
    }

    public RespuestaTo(String mensaje) {
        this.mensaje = mensaje;
    }

    private String mensaje;
}
