/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.constante;

/**
 *
 * @author christian
 */
public class ConstanteSistema {

    public static final String CORRECTO = "transacción exitosa";
    public static final String REGISTRO_NO_EXISTE= "Registro no existente.";
    public static final String LIMITE_RETIRO_EXCEDENTE = "El valor a retirar es mayor al límite diario ";
    public static final String SALDO_INSUFICIENTE = "El saldo es insuficiente ";
}
