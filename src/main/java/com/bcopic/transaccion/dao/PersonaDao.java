/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.dao;

import com.bcopic.transaccion.dao.generico.GenericoDao;
import com.bcopic.transaccion.modelo.Persona;
import org.springframework.stereotype.Repository;

/**
 *
 * @author christian
 */
@Repository
public class PersonaDao extends GenericoDao<Persona> {

    public PersonaDao() {
        super(Persona.class);
    }

}
