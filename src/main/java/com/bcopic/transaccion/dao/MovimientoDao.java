/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.dao;

import com.bcopic.transaccion.dao.generico.GenericoDao;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Movimiento;
import com.bcopic.transaccion.to.ReporteTo;
import com.bcopic.transaccion.util.SelectNativoUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author christian
 */
@Repository
public class MovimientoDao extends GenericoDao<Movimiento> {

    private static final String OBTENER_ULTIMO_POR_CUENTA = "select m from Movimiento m WHERE m.idCuenta = :idCuenta order by m.id desc";
    private static final String LISTAR_POR_CUENTA_FECHA_TIPO_MOVIMIENTO = "select m from Movimiento m WHERE m.idCuenta = :idCuenta and m.fecha >= :fechaDesde and m.fecha <= :fechaHasta AND m.idTipoMovimiento = :idTipo";
    private static final String LISTAR_REPORTE_POR_FECHA_IDENTIFICACION = "select m.mov_fecha, p.per_nombre, c.cue_id, cat.cat_nombre, c.cue_saldo_inicial, c.cue_estado,"
            + " (select cat_codigo from tbl_catalogo where cat_id = m.cat_tipo_movimiento_id) as movimiento,"
            + " m.mov_valor, m.mov_saldo"
            + " from tbl_movimiento m, tbl_cuenta c, tbl_cliente cl, tbl_persona p, tbl_catalogo cat"
            + " where m.cue_id = c.cue_id and c.cli_id=cl.cli_id and cl.per_id = p.per_id and c.cat_tipo_cuenta_id=cat.cat_id"
            + " and p.per_identificacion = ? AND m.mov_fecha >= ? AND m.mov_fecha <= ? order by m.mov_fecha desc";

    public MovimientoDao() {
        super(Movimiento.class);
    }

    public Movimiento obtenerUltimoPorCuenta(Long idCuenta) throws SistemaExcepcion {
        Map<String, Object> parametro = new HashMap<>();
        parametro.put("idCuenta", idCuenta);
        return obtenerPorQuery(OBTENER_ULTIMO_POR_CUENTA, parametro);
    }

    public List<Movimiento> listarPorCuentaFechaTipoMovimiento(Long idCuenta, Date fechaDesde, Date fechaHasta, Long idTipo) throws SistemaExcepcion {
        Map<String, Object> parametro = new HashMap<>();
        parametro.put("idCuenta", idCuenta);
        parametro.put("fechaDesde", fechaDesde);
        parametro.put("fechaHasta", fechaHasta);
        parametro.put("idTipo", idTipo);
        return listarPorQuery(LISTAR_POR_CUENTA_FECHA_TIPO_MOVIMIENTO, parametro);
    }

    public List<ReporteTo> listarPorFechaIdentificacion(Date fechaDesde, Date fechaHasta, String identificacion) throws SistemaExcepcion {
        Query query = getEntityManager().createNativeQuery(LISTAR_REPORTE_POR_FECHA_IDENTIFICACION);
        query.setParameter(1, identificacion);
        query.setParameter(2, fechaDesde);
        query.setParameter(3, fechaHasta);
        return SelectNativoUtil.listarPorSentenciaNativaSql(query, ReporteTo.class);
    }

}
