/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.dao;

import com.bcopic.transaccion.dao.generico.GenericoDao;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Parametro;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;

/**
 *
 * @author christian
 */
@Repository
public class ParametroDao extends GenericoDao<Parametro> {

    private static final String OBTENER_POR_CODIGO = "select p from Parametro p where p.codigo = :codigo";

    public ParametroDao() {
        super(Parametro.class);
    }

    public Parametro obtenerPorCodigo(String codigo) throws SistemaExcepcion {
        Map<String, Object> parametro = new HashMap<>();
        parametro.put("codigo", codigo);
        return obtenerPorQuery(OBTENER_POR_CODIGO, parametro);
    }

}
