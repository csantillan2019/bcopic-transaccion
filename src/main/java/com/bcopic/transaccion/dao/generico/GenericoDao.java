/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.dao.generico;

import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author christian
 */
@Repository
public abstract class GenericoDao<T> {

    @PersistenceContext
    private EntityManager entityManager;

    private Class<T> entity;

    public GenericoDao(Class<T> entity) {
        this.entity = entity;
    }

    public void guardar(T t) throws SistemaExcepcion {
        try {
            entityManager.merge(t);
        } catch (RuntimeException e) {
            throw new SistemaExcepcion(e);
        }
    }

    public T guardarRetorno(T t) throws SistemaExcepcion {
        try {
            T tipo = entityManager.merge(t);
            entityManager.flush();
            return tipo;
        } catch (RuntimeException e) {
            throw new SistemaExcepcion(e);
        }
    }

    public void guardarBloque(List<T> list) throws SistemaExcepcion {
        try {
            list.forEach(en -> entityManager.merge(en));
        } catch (RuntimeException e) {
            throw new SistemaExcepcion(e);
        }
    }

    public List<T> listarTodo() {
        return getEntityManager().createQuery("select c from " + entity.getSimpleName() + " c").getResultList();
    }

    public List<T> listarPorQuery(String jpaQl, Map<String, Object> parametro) throws SistemaExcepcion {
        try {
            if (Objects.isNull(parametro) || parametro.isEmpty()) {
                return getEntityManager().createQuery(jpaQl).getResultList();
            } else {
                Query query = getEntityManager().createQuery(jpaQl);
                parametro.forEach((key, value) -> {
                    query.setParameter(key, value);
                });
                return query.getResultList();
            }

        } catch (RuntimeException e) {
            throw new SistemaExcepcion(e);
        }
    }

    public T obtenerPorQuery(String jpaQl, Map<String, Object> parametro) throws SistemaExcepcion {
        try {
            List<T> listaDatos;
            if (Objects.isNull(parametro) || parametro.isEmpty()) {
                listaDatos = getEntityManager().createQuery(jpaQl).getResultList();
                if (Objects.isNull(listaDatos) || listaDatos.isEmpty()) {
                    return null;
                } else {
                    return listaDatos.get(0);
                }
            } else {
                Query query = getEntityManager().createQuery(jpaQl);
                parametro.forEach((key, value) -> {
                    query.setParameter(key, value);
                });
                listaDatos = query.getResultList();
                if (Objects.isNull(listaDatos) || listaDatos.isEmpty()) {
                    return null;
                } else {
                    return listaDatos.get(0);
                }
            }

        } catch (RuntimeException e) {
            throw new SistemaExcepcion(e);
        }
    }

    public T obtenerPorLlavePrimaria(Object id) {
        return entityManager.find(entity, id);
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

}
