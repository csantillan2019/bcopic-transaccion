/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.dao;

import com.bcopic.transaccion.dao.generico.GenericoDao;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Cliente;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

/**
 *
 * @author christian
 */
@Repository
public class ClienteDao extends GenericoDao<Cliente> {

    private static final String LISTAR_ACTIVO = "select c from Cliente c where c.estado= true";
    private static final String OBTENER_POR_IDENTIFICACION = "select c from Cliente c where c.persona.identificacion = :identificacion";

    public ClienteDao() {
        super(Cliente.class);
    }

    public List<Cliente> listarActivo() throws SistemaExcepcion {
        return listarPorQuery(LISTAR_ACTIVO, null);
    }

    public Cliente obtenerPorIdentificacion(String identificacion) throws SistemaExcepcion {
        Map<String, Object> parametro = new HashMap<>();
        parametro.put("identificacion", identificacion);
        return obtenerPorQuery(OBTENER_POR_IDENTIFICACION, parametro);
    }

}
