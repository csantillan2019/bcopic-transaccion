/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.dao;

import com.bcopic.transaccion.dao.generico.GenericoDao;
import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import com.bcopic.transaccion.modelo.Cliente;
import com.bcopic.transaccion.modelo.Cuenta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

/**
 *
 * @author christian
 */
@Repository
public class CuentaDao extends GenericoDao<Cuenta> {

    private static final String LISTAR_ACTIVO = "select c from Cuenta c where c.estado= true";

    private static final String OBTENER_POR_CLIENTE = "select c from Cuenta c where c.cliente= :cliente";

    public CuentaDao() {
        super(Cuenta.class);
    }

    public List<Cuenta> listarActivo() throws SistemaExcepcion {
        return listarPorQuery(LISTAR_ACTIVO, null);
    }

    public Cuenta obtenerPorCliente(Cliente cliente) throws SistemaExcepcion {
        Map<String, Object> parametro = new HashMap<>();
        parametro.put("cliente", cliente);
        return obtenerPorQuery(OBTENER_POR_CLIENTE, parametro);
    }

}
