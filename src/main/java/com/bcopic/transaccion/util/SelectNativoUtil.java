package com.bcopic.transaccion.util;

import com.bcopic.transaccion.excepcion.SistemaExcepcion;
import java.util.List;
import javax.persistence.Query;

/**
 * <b>
 * Descripción de clase.
 * </b>
 *
 * @author christian
 * <p>
 * [$Author: christian $, $Date: 2016-12-31
 * </p>
 */
public class SelectNativoUtil {

    public static <T> List<T> listarPorSentenciaNativaSql(final Query query, final Class claseRetorno)
            throws SistemaExcepcion {
        try {
            return (List<T>) SqlUtil.retornarDatos(query.getResultList(), claseRetorno);
        } catch (RuntimeException e) {
            throw new SistemaExcepcion(e);
        }
    }

}
