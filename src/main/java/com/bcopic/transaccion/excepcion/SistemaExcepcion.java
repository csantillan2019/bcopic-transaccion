/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bcopic.transaccion.excepcion;

/**
 *
 * @author christian
 */
public class SistemaExcepcion extends Exception {
    
    public SistemaExcepcion(String mensaje) {
        super(mensaje);
    }
    
    public SistemaExcepcion(Throwable throwable) {
        super(throwable);
    }
    
    public SistemaExcepcion(String mensaje, Throwable throwable) {
        super(mensaje, throwable);
    }
    
}
